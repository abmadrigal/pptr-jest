# pptr-jest

# Amazon Product Details page UI automation


This is project using Puppeteer, Jest and Jest-Stare to automate UI

## Install NPM & Node (includes both /usr/bin/nodejs and /usr/bin/npm)

## Install yarn

brew install yarn


## Download dependencies (ignore platform specific WARNings at the end)

yarn install

## Install Puppeteer

yarn add puppeteer

## Install Jest

yarn add --dev jest

## Install Jest-stare

Follow steps at https://www.npmjs.com/package/jest-stare

## Run script for testing only

yarn test

## Run script for testing and reporting

yarn test-stare
