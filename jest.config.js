module.exports = {
    preset: "jest-puppeteer",
    globals: {
        URL: "https://amazon.com"
    },
    testMatch: [
        "**/test/**/*.test.js"
    ],
    verbose: true
};