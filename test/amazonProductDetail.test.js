const timeout = 10000;

beforeEach(async() => {
    await page.goto(URL + '/POC-Ventral-Helmet-Cycling-Apophyllite/dp/B0848V4WQ4', { waitUntil: "domcontentloaded" });
    await page.setViewport({ width: 1940, height: 1080 });
});

describe("Validates title and header of the homepage", () => {
    test("Validates page title", async() => {
        let title = await page.title();
        expect(title).toBe('Amazon.com : POC, Ventral Air Spin Bike Helmet for Road Cycling : Sports & Outdoors');
        await page.screenshot({ path: './evidence/test1/' + getFormattedTime() + '.png' });
    }, timeout);

    test("Validates page header", async() => {
        let pT = await page.$("#productTitle");
        let productTitle = await page.evaluate(pT => pT.innerText, pT);
        expect(productTitle).toBe("POC, Ventral Air Spin Bike Helmet for Road Cycling");
    }, timeout);

    test("Validates Add to Cart is visible", async() => {
        await page.waitForSelector('#partialStateBuybox');
        let addToListDropdown = await page.$('#wishListMainButton-announce');
        let addToListText = await page.evaluate(addToListDropdown => addToListDropdown.innerText, addToListDropdown);
        expect(addToListText).toBe('Add to List');
        let addButton = await page.$('#add-to-cart-button');
        let textButton = await page.evaluate(addButton => addButton.value, addButton);
        expect(textButton).toBe("Add to Cart");
    }, timeout);

    test("Validates share icons", async() => {
        await page.waitForSelector('#tell-a-friend');
        //validates share to from text
        let shareLink = await page.$('#swfMailTo');
        let shareLinkDetail = await page.evaluate(shareLink => shareLink.getAttribute('href'), shareLink);
        expect(shareLinkDetail).toEqual(expect.stringMatching(/^mailto:.*/));
        //validates share to from icon
        let shareLinkIcon = await page.$('#swfImageMailTo');
        let shareLinkDetailIcon = await page.evaluate(shareLinkIcon => shareLinkIcon.getAttribute('href'), shareLinkIcon);
        expect(shareLinkDetailIcon).toEqual(expect.stringMatching(/^mailto:.*/));
        //validates facebook share to from icon
        let shareLinkFb = await page.$('#facebook');
        let shareLinkDetailFb = await page.evaluate(shareLinkFb => shareLinkFb.getAttribute('href'), shareLinkFb);
        expect(shareLinkDetailFb).toEqual(expect.stringMatching(/www\.facebook\.com/));
        //validates twitter share to from icon
        let shareLinkTw = await page.$('#twitter');
        let shareLinkDetailTw = await page.evaluate(shareLinkTw => shareLinkTw.getAttribute('href'), shareLinkTw);
        expect(shareLinkDetailTw).toEqual(expect.stringMatching(/tweet/));
        //validates pinterest share to from icon
        let shareLinkPt = await page.$('#pinterest');
        let shareLinkDetailPt = await page.evaluate(shareLinkPt => shareLinkPt.getAttribute('href'), shareLinkPt);
        expect(shareLinkDetailPt).toEqual(expect.stringMatching(/pinterest\.com/));
    }, timeout);
});

describe("Validates CTAs of the page", () => {
    test("Add to Cart CTA", async() => {
        selectSize();
        let shoppingCart = await page.$('#nav-cart-count');
        let shoppingCartCount = await page.evaluate(shoppingCart => shoppingCart.innerText, shoppingCart);
        expect(shoppingCartCount).toBe('0');
        await page.waitForSelector('#buy-now-button', { visible: true });
        await page.waitForSelector('#add-to-cart-button');
        await page.click('#add-to-cart-button');
        await page.waitForNavigation({ waitUntil: 'networkidle0' });
        let scTitle = await page.title();
        expect(scTitle).toBe('Amazon.com Shopping Cart');
        shoppingCart = await page.$('#nav-cart-count');
        shoppingCartCount = await page.evaluate(shoppingCart => shoppingCart.innerText, shoppingCart);
        expect(shoppingCartCount).toBe('1');
    }, timeout);

    test("Buy Now CTA", async() => {
        selectSize();
        await page.waitForSelector('#buy-now-button', { visible: true });
        await page.click('#buy-now-button');
        await page.waitForNavigation({ waitUntil: 'networkidle0' });
        let pageTitle = await page.title();
        expect(pageTitle).toBe('Amazon Sign-In');
    }, timeout);
});

// Function to get formatted time

getFormattedTime = () => {
    var today = new Date();
    var y = today.getFullYear();
    // JavaScript months are 0-based.
    var m = today.getMonth() + 1;
    var d = today.getDate();
    var h = today.getHours();
    var mi = today.getMinutes();
    var s = today.getSeconds();
    return y + "-" + m + "-" + d + "-" + h + "-" + mi + "-" + s;
};

//Function to select size

const selectSize = async() => {
    await page.waitForSelector('#dropdown_selected_size_name');
    await page.click('#dropdown_selected_size_name');
    await page.waitForSelector('#size_name_1');
    await page.click('#size_name_1');
};