const puppeteer = require('puppeteer');
describe('Verify POC helmet product detail Page', () => {
    var browser, page;
    var url = 'https://www.amazon.com/POC-POCito-Fluorescent-Yellow-X-Small/dp/B07WW8V4YH';
    beforeEach(async() => {
        browser = await puppeteer.launch({ headless: false });
        page = await browser.newPage();
    })
    afterEach(() => {
        browser.close();
    });
    test('Title == Pronto Tools', async() => {
        await page.goto(url);
        await expect(page.title).resolves.toMatch('Amazon.com : POC, POCito Omne Spin Helmet, Fluorescent Yellow/Green, X-Small : Sports & Outdoors');
        // const title = await page.title();
        // expect(title).tobe('Amazon.com : POC, POCito Omne Spin Helmet, Fluorescent Yellow/Green, X-Small : Sports & Outdoors');
    });
    test("Center == Tools for Your Growing Business", async() => {
        await page.goto(url);
        const center = await page.$eval('#productTitle', e => e.innerText);
        expect(center).toBe("POC, POCito Omne Spin");
    });
});